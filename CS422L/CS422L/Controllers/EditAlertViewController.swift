//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit
import CoreData

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: FlashcardEntity = FlashcardEntity()
    var index: Int = Int()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        self.dismiss(animated: false, completion: nil)
        do {
            context.delete(card)
            try context.save()
        } catch{
            print(error)
        }
        parentVC?.cards.remove(at: index)
        parentVC?.tableView.reloadData()
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            //do something / save the edits
        })
        do {
            if(termEditText.text?.isEmpty == false && definitionEditText.text?.isEmpty == false){
            card.term = termEditText.text
            card.definition = definitionEditText.text
            }
            try context.save()
        } catch{
            print(error)
        }
        parentVC?.tableView.reloadData()
    }
    
}
